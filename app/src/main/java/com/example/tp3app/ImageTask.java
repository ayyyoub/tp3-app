package com.example.tp3app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.net.URL;

public class ImageTask extends AsyncTask<ImageView, Void, Bitmap> {

    ImageView image;
    Team team;

    public ImageTask(ImageView image, Team team) {
        this.image = image;
        this.team = team;
    }

    @Override
    protected Bitmap doInBackground(ImageView... imageViews) {

        Bitmap bmp = null;

        try {
            URL url = new URL(this.team.getTeamBadge());
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            Log.d("error_badge",e.toString());
        }

        return bmp;
    }

    @Override
    protected void onPostExecute(Bitmap result) {

        image.setImageBitmap(result);

    }
}