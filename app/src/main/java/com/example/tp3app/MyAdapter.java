package com.example.tp3app;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private Context context;

    public MyAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater myInflater = LayoutInflater.from(context);
        View view = myInflater.inflate(R.layout.team_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        Team team = MainActivity.DB.getAllTeams().get(position);

        new ImageTask(holder.image, team).execute();
        holder.teamName.setText(team.getName());
        holder.leagueName.setText(team.getLeague());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(context, TeamActivity.class);
                myIntent.putExtra(Team.TAG, MainActivity.DB.getAllTeams().get(position));
                context.startActivity(myIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return MainActivity.DB.getAllTeams().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout parentLayout;

        ImageView image;
        TextView teamName;
        TextView leagueName;


        public ViewHolder(@NonNull View itemView) {

            super(itemView);

            parentLayout = itemView.findViewById(R.id.parent_layout);

            image = itemView.findViewById(R.id.image);
            teamName = itemView.findViewById(R.id.teamName);
            leagueName = itemView.findViewById(R.id.leagueName);

        }
    }
}
