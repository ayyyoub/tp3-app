package com.example.tp3app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
//import com.example.tp3.webservice.WebServiceGet;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;
    private ImageView imageBadge;

    private JSONResponseHandlerTeam responseHandlerTeam;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private Team team;

    public static String DONE = "Executed";
    public static String FAILED = "Failed";

    private FetchTeamContent runningTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        this.responseHandlerTeam = new JSONResponseHandlerTeam(team);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.leagueName);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        updateView();

        final Button update = (Button) findViewById(R.id.update);

        update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (runningTask != null)
                    runningTask.cancel(true);

                runningTask = new FetchTeamContent();
                runningTask.execute();

            }
        });

    }

    @Override
    public void onBackPressed() {

        // TODO : prepare result for the main activity

        // Save into the DB
        MainActivity.DB.updateTeam(team);

        // Destroy the running thread
//        runningTask.cancel(true);

        // Update the listview
        MainActivity.adapter.notifyDataSetChanged();

        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        new ImageTask(imageBadge, this.team).execute();
    }

    public static void displayDialog(String title, String message, Context context, int time){

        final AlertDialog alert = new AlertDialog.Builder(context).create();
        alert.setTitle(title);
        alert.setMessage(message);
        alert.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert.dismiss();
            }
        }, time);
    }

    public static String loadTeamContent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        try {

//            Verify if we are currently connected
            if (activeNetwork != null) {

                /**
                 * Fetch the image
                 */
                URL url = WebServiceUrl.buildSearchTeam(team.getName());
//                Log.d("url content", url.toString());
                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStream(response);

                } else {
                    TeamActivity.displayDialog("Serveurs indisponibles","Veuillez réessayer plus tard !", context, 10000);
                    return TeamActivity.FAILED;
                }

            } else {
                TeamActivity.displayDialog("Connection impossible","Veuillez vous connectez à internet !", context, 10000);
                return TeamActivity.FAILED;
            }

        } catch (Exception e) {
            return TeamActivity.FAILED;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }

    }

    public static String loadTeamLastEvent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        try {

//            Verify if we are currently connected
            if (activeNetwork != null) {

                /**
                 * Fetch the last event
                 */
                URL url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection != null && urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStreamLastEvent(response);

                } else {
                    TeamActivity.displayDialog("Serveurs indisponibles","Veuillez réessayer plus tard !", context, 10000);
                    return TeamActivity.FAILED;
                }

            } else {
                TeamActivity.displayDialog("Connection impossible","Veuillez vous connectez à internet !", context, 10000);
                return TeamActivity.FAILED;
            }

        } catch (Exception e) {
            return TeamActivity.FAILED;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }

    }

    public static String loadTeamRank(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        try {

//            Verify if we are currently connected
            if (activeNetwork != null) {

                /**
                 * Fetch the last event
                 */
                URL url = WebServiceUrl.buildGetRanking(team.getIdLeague());
//                Log.d("url_rank", url.toString());
                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStreamRank(response);

                } else {
                    TeamActivity.displayDialog("Serveurs indisponibles","Veuillez réessayer plus tard !", context, 10000);
                    return TeamActivity.FAILED;
                }

            } else {
                TeamActivity.displayDialog("Connection impossible","Veuillez vous connectez à internet !", context, 10000);
                return TeamActivity.FAILED;
            }

        } catch (Exception e) {
            return TeamActivity.FAILED;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }

    }

    private final class FetchTeamContent extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            // Fetch content such as image, ect ...
            String s0 = loadTeamContent(team, responseHandlerTeam, TeamActivity.this);

            // Fetch last event
            String s1 = loadTeamLastEvent(team, responseHandlerTeam, TeamActivity.this);

            if (s0.equals(TeamActivity.DONE) && s1.equals(TeamActivity.DONE)) {
                return TeamActivity.DONE;
            } else {
                return TeamActivity.FAILED;
            }
        }

        @Override
        protected void onPostExecute(String result) {

            updateView();

            Log.d("BETA12599","View updated !");
        }
    }
}
